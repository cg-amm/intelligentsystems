# -*- coding: utf-8 -*-
"""
Created on Sat Dec 22 14:22:49 2018

@author: finn-
"""

import seaborn as sns; sns.set()
import matplotlib.pyplot as plt

with open(r'C:\Users\finn-\Documents\MEGA\TUHH\Master\ISM\GIT\Includes\ISIC2018_Task3_Training_GroundTruth.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    labels1 = []
    labels2 = []
    labels3 = []
    labels4 = []
    labels5 = []
    labels6 = []
    labels7 = []
    
    for row in readCSV:
#        print(row[1],row[2],row[3],row[4],row[5],row[6],row[7])
#         labels = [row[1],row[2],row[3],row[4],row[5],row[6],row[7]]
         label1 = [row[1]]
         labels1.append(label1) 
         label2 = [row[2]]
         labels2.append(label2)
         label3 = [row[3]]
         labels3.append(label3)
         label4 = [row[4]]
         labels4.append(label4)
         label5 = [row[5]]
         labels5.append(label5)
         label6 = [row[6]]
         labels6.append(label6)
         label7 = [row[7]]
         labels7.append(label7)
    
    labels = [labels1,labels2,labels3,labels4,labels5,labels6,labels7]
    labels = list(map(list, zip(*labels)))
    labels = np.asarray(labels)
    labels = labels[1:len(labels),:]

    labelsArray = []
    for i in range(len(labels)):
        temp = np.argmax(labels[i,:]) + 1
        labelsArray.append(temp)
        
        
com = np.load(r'C:\Users\finn-\Documents\MEGA\TUHH\Master\ISM\GIT\Documents/' + featurematrix + '.npy' )
comtest = np.c_[com,labelsArray]

x1 = []
x2 = []
x3 = [] 
x4 = []
x5 = []
x6 = []
x7 = [] 

y1 = [] 
y2 = []
y3 = []
y4 = [] 
y5 = []
y6 = []
y7 = []
feature1 = 0
feature2 = 2 
for i in range(len(comtest)):
    if comtest[i,-1] == 1:
        x1.append(comtest[i,feature1].astype(np.float))
        y1.append(comtest[i,feature2].astype(np.float))
    elif comtest[i,-1] == 2:
         x2.append(comtest[i,feature1].astype(np.float))
         y2.append(comtest[i,feature2].astype(np.float))
    elif comtest[i,-1] == 3:
         x3.append(comtest[i,feature1].astype(np.float))
         y3.append(comtest[i,feature2].astype(np.float))
    elif comtest[i,-1] == 4:
         x4.append(comtest[i,feature1].astype(np.float))
         y4.append(comtest[i,feature2].astype(np.float))
    elif comtest[i,-1] == 5:
         x5.append(comtest[i,feature1].astype(np.float))
         y5.append(comtest[i,feature2].astype(np.float))
    elif comtest[i,-1] == 6:
         x6.append(comtest[i,feature1].astype(np.float))
         y6.append(comtest[i,feature2].astype(np.float))
    elif comtest[i,-1] == 7:
         x7.append(comtest[i,feature1].astype(np.float))
         y7.append(comtest[i,feature2].astype(np.float))


MEL = plt.scatter(x1, y1,)
NV = plt.scatter(x2, y2,)
BCC = plt.scatter(x3, y3,)
AKIEC = plt.scatter(x4, y4,)
BKL = plt.scatter(x5, y5,)
DF = plt.scatter(x6, y6,)
VASC = plt.scatter(x7, y7,)

plt.legend((MEL, NV, BCC, AKIEC, BKL, DF, VASC),
           ('MEL', 'NV', 'BCC', 'AKIEC', 'BKL', 'DF', 'VASC'),
           scatterpoints=1,
           loc='lower right',
           ncol=3,
           fontsize=8)