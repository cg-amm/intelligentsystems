#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 12:43:37 2018

@author: veraschulz
"""
#selection of most common classifiers and model function
import numpy as np
import pandas as pd
import numpy as np
import csv
import os
os.chdir(r'C:\Users\finn-\Desktop\TEsting\Includes')
# ------ Load Data -----------
# append features and labels of test dataset

import configparser

# open the configuration file and initialise variables
config = configparser.ConfigParser()
config.read('../Documents/init.ini')

featurematrix = config['Classifier']['FeatureMatrix']
path = (r"G:\ISM Projekt\ISIC2018_Task3_Training_Input")
def initialize(path):
     # load image list

     os.chdir(path)
     images=[]
     for image in os.listdir(path):
          if image.endswith('.jpg') or image.endswith('.JPG'):
               images.append(image)
     
     return images 



images = initialize(path)
os.chdir(r'C:\Users\finn-\Desktop\TEsting\Includes')
# build train and test dataset
from sklearn.model_selection import train_test_split

# get groundtruth data from csv file and put it into a matrix
# first line is still the definition of the label (so skip one line)
with open('ISIC2018_Task3_Training_GroundTruth.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    labels1 = []
    labels2 = []
    labels3 = []
    labels4 = []
    labels5 = []
    labels6 = []
    labels7 = []
    
    for row in readCSV:
#        print(row[1],row[2],row[3],row[4],row[5],row[6],row[7])
#         labels = [row[1],row[2],row[3],row[4],row[5],row[6],row[7]]
         label1 = [row[1]]
         labels1.append(label1) 
         label2 = [row[2]]
         labels2.append(label2)
         label3 = [row[3]]
         labels3.append(label3)
         label4 = [row[4]]
         labels4.append(label4)
         label5 = [row[5]]
         labels5.append(label5)
         label6 = [row[6]]
         labels6.append(label6)
         label7 = [row[7]]
         labels7.append(label7)
    
    labels = [labels1,labels2,labels3,labels4,labels5,labels6,labels7]
    labels = list(map(list, zip(*labels)))
    labels = np.asarray(labels)
    labels = labels[1:len(labels),:]

    labelsArray = []
    for i in range(len(labels)):
        temp = np.argmax(labels[i,:]) + 1
        labelsArray.append(temp)
## Load Featruematrix
com = np.load('../Documents/' + featurematrix)


## Split Train/Test set
# read in the csv file
file = open('../Documents/val_split_info.csv', "r")
csv_reader = csv.reader(file, delimiter=",")
validation =[]
i = 0
for row in csv_reader:
   row = row[0] + '.jpg'  # for comparison with images
   validation.append(row)
   i = i + 1
file.close()

# get the indices of train and test set
validation = validation[1:len(validation)]
valindex = []
trainindex = []
for i in range(len(validation)):
    for j in range(len(images)):
        if validation[i] == images[j]: 
            valindex.append(j)


# fill the train and test set            
val_labels = []
val = [] 
for ind in valindex: 
    val_labels.append(labelsArray[ind])
    val.append(com[ind])
    
train = [] 
train_labels = [] 
for ind in trainindex: 
    train_labels.append(labelsArray[ind])
    train.append(com[ind])

#rename the found train and test set
test = np.asarray(val)
test_labels =val_labels
train = np.asarray(train)


# Split our data #feature represents the feature vector
train1, test1, train_labels1, test_labels1 = train_test_split(com,
                                                          labelsArray,
                                                          test_size=2500/10000
                                                          )


from sklearn.metrics import accuracy_score
import numpy as np
import matplotlib.pyplot as plt


def run_model(model, alg_name):
    # build the model on training data
    model.fit(train, train_labels)

    # make predictions for test data
    y_pred = model.predict(test)
    # calculate the accuracy score
    accuracy =  accuracy_score(test_labels, y_pred) * 100
    return y_pred, accuracy

# ---- Decision Tree -----------
#from sklearn import tree
#
#model = tree.DecisionTreeClassifier(criterion='entropy', max_depth=5)
#y_pred, accuracy = run_model(model, "Decision Tree")
#
## ----- Random Forest ---------------Bagging
#from sklearn.ensemble import RandomForestClassifier
#
#model = RandomForestClassifier(n_estimators=10)
#run_model(model, "Random Forest")

# ----- xgboost ------------
# install xgboost
# 'pip install xgboost' or https://stackoverflow.com/questions/33749735/how-to-install-xgboost-package-in-python-windows-platform/39811079#39811079

#from xgboost import XGBClassifier

#model = XGBClassifier()
#run_model(model, "XGBoost")

# ------ SVM Classifier ----------------
from sklearn.svm import SVC
model = SVC(C=1, kernel='rbf', gamma=0.001)
run_model(model, "SVM Classifier")

# -------- Nearest Neighbors ----------
#from sklearn import neighbors
#model = neighbors.KNeighborsClassifier()
#run_model(model, "Nearest Neighbors Classifier")
#
## ---------- SGD Classifier -----------------
#from sklearn.linear_model import SGDClassifier
#from sklearn.multiclass import OneVsRestClassifier
#
#model = OneVsRestClassifier(SGDClassifier())
#run_model(model, "SGD Classifier")
#
## --------- Gaussian Naive Bayes ---------
#from sklearn.naive_bayes import GaussianNB
#
#model = GaussianNB()
#run_model(model, "Gaussian Naive Bayes")
#
## ----------- Neural network - Multi-layer Perceptron  ------------
from sklearn.neural_network import MLPClassifier

model = MLPClassifier()
run_model(model, " MLP Neural network ")