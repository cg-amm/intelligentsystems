#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 12:43:37 2018

@author: veraschulz
"""
#selection of most common classifiers and model function
import numpy as np
import pandas as pd
import numpy as np
import csv
import os

# ------ Load Data -----------
# append features and labels of test dataset

import configparser

# open the configuration file and initialise variables
config = configparser.ConfigParser()
config.read('../Documents/init.ini')                                          ####### adjust the path!!#######

featurematrix = config['Classifier']['FeatureMatrix']
#com = np.load('../Documents/comcolor_with_Hairremoval_and_with_Segmentmole.npy')
#com = np.load('../Documents/comcolor_with_Hairremoval_and_without_Segmentmole.npy')
#com = np.load('../Documents/comcolor_without_Hairremoval_and_with_Segmentmole.npy')
#com = np.load('../Documents/comcolor_without_Hairremoval_and_without_Segmentmole.npy')
#
#com = np.load('../Documents/com_with_Hairremoval_and_with_Segmentmole.npy')
#com = np.load('../Documents/com_with_Hairremoval_and_without_Segmentmole.npy')
#com = np.load('../Documents/com_without_Hairremoval_and_with_Segmentmole.npy')
#com = np.load('../Documents/com_without_Hairremoval_and_without_Segmentmole.npy')
#
#com = np.load('../Documents/color_with_Hairremoval_and_with_Segmentmole.npy')
#com = np.load('../Documents/color_with_Hairremoval_and_without_Segmentmole.npy')
#com = np.load('../Documents/color_without_Hairremoval_and_with_Segmentmole.npy')
#com = np.load('../Documents/color_without_Hairremoval_and_without_Segmentmole.npy')
#
# new
#com = np.load('../Documents/comcolor_with_Hairremoval_and_with_Segmentmole_v2.npy')
#com = np.load('../Documents/comcolor_with_Hairremoval_and_without_Segmentmole_v2.npy')
#com = np.load('../Documents/comcolor_without_Hairremoval_and_with_Segmentmole_v2.npy')
#com = np.load('../Documents/comcolor_without_Hairremoval_and_without_Segmentmole_v2.npy')
#
#com = np.load('../Documents/color_with_Hairremoval_and_with_Segmentmole_v2.npy')
#com = np.load('../Documents/color_with_Hairremoval_and_without_Segmentmole_v2.npy')
#com = np.load('../Documents/color_without_Hairremoval_and_with_Segmentmole_v2.npy')
#com = np.load('../Documents/color_without_Hairremoval_and_without_Segmentmole_v2.npy')
#
#com = np.load('../Documents/com_with_Hairremoval_and_with_Segmentmole_v2.npy')
#com = np.load('../Documents/com_with_Hairremoval_and_without_Segmentmole_v2.npy')
#com = np.load('../Documents/com_without_Hairremoval_and_with_Segmentmole_v2.npy')
#com = np.load('../Documents/com_without_Hairremoval_and_without_Segmentmole_v2.npy')
#
#com = np.load('../Documents/comcolor_with_Hairremoval_and_with_Segmentmole_hull.npy')
#com = np.load('../Documents/com_with_Hairremoval_and_with_Segmentmole_hull.npy')
#com = np.load('../Documents/color_with_Hairremoval_and_with_Segmentmole_hull.npy')
#



# build train and test dataset
from sklearn.model_selection import train_test_split

# get groundtruth data from csv file and put it into a matrix
# first line is still the definition of the label (so skip one line)
with open('ISIC2018_Task3_Training_GroundTruth.csv') as csvfile:  ####### adjust the path!!#######
    readCSV = csv.reader(csvfile, delimiter=',')
    labels1 = []
    labels2 = []
    labels3 = []
    labels4 = []
    labels5 = []
    labels6 = []
    labels7 = []
    
    for row in readCSV:
#        print(row[1],row[2],row[3],row[4],row[5],row[6],row[7])
#         labels = [row[1],row[2],row[3],row[4],row[5],row[6],row[7]]
         label1 = [row[1]]
         labels1.append(label1) 
         label2 = [row[2]]
         labels2.append(label2)
         label3 = [row[3]]
         labels3.append(label3)
         label4 = [row[4]]
         labels4.append(label4)
         label5 = [row[5]]
         labels5.append(label5)
         label6 = [row[6]]
         labels6.append(label6)
         label7 = [row[7]]
         labels7.append(label7)
    
    labels = [labels1,labels2,labels3,labels4,labels5,labels6,labels7]
    labels = list(map(list, zip(*labels)))
    labels = np.asarray(labels)
    labels = labels[1:len(labels),:]

    labelsArray = []
    for i in range(len(labels)):
        temp = np.argmax(labels[i,:]) + 1
        labelsArray.append(temp)

#com = np.load('../Documents/' + featurematrix + '.npy')  ####### adjust the path!!#######

def initialize(path):
     # load image list
     os.chdir(path)
     images=[]
     for image in os.listdir(path):
          if image.endswith('.jpg') or image.endswith('.JPG'):
               images.append(image)
     
     return images
                                  ####### adjust the path!!#######
path = (r"C:\Users\alexa\Documents\ISM\ISIC2018_Task3_Training_Input")
allImages = initialize(path)

testImages = []
with open('val_split_info.csv') as csvDataFile:        ####### adjust the path!!#######
    csvReader = csv.reader(csvDataFile)
    for row in csvReader:
        testImages.append(row[0])

test_labels = []
test = []
train_labels = []
train = []

for testImage in allImages:
     if testImage in testImages:
          ind = allImages.index(testImage)
          test_labels.append(labelsArray[ind])
          test.append(com[ind])
     else:
          ind = allImages.index(testImage)
          train_labels.append(labelsArray[ind])
          train.append(com[ind])

print(len(train))
print(len(test))
print(len(train_labels))
print(len(test_labels))

## Split our data #feature represents the feature vector
# train, test, train_labels, test_labels = train_test_split(com,
                                                        #   labelsArray,
                                                        #   test_size=2500/10000,
                                                        #   random_state=3)

from sklearn.metrics import accuracy_score
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report


def run_model(model, alg_name):
    # build the model on training data
    model.fit(train, train_labels)

    # make predictions for test data
    y_pred_proba = model.predict_proba(test)
    # calculate the accuracy score
    y_pred = model.predict(test)
    accuracy =  accuracy_score(test_labels, y_pred) * 100
#    print(report = classification_report(test_labels, y_pred))
    return y_pred_proba, accuracy

## ---- Decision Tree -----------
#from sklearn import tree
#
#model = tree.DecisionTreeClassifier(criterion='entropy', max_depth=5)
#y_pred, accuracy = run_model(model, "Decision Tree")
#
## ----- Random Forest ---------------Bagging
#from sklearn.ensemble import RandomForestClassifier
#
#model = RandomForestClassifier(n_estimators=10)
#y_pred, accuracy = run_model(model, "Random Forest")

# ----- xgboost ------------
# install xgboost
# 'pip install xgboost' or https://stackoverflow.com/questions/33749735/how-to-install-xgboost-package-in-python-windows-platform/39811079#39811079

# from xgboost import XGBClassifier
# model = XGBClassifier()
# run_model(model, "XGBoost")

# ------ SVM Classifier ----------------
# from sklearn.svm import SVC
# model = SVC()
# y_pred, accuracy = run_model(model, "SVM Classifier")

## -------- Nearest Neighbors ----------
#from sklearn import neighbors
#model = neighbors.KNeighborsClassifier()
#y_pred, accuracy = run_model(model, "Nearest Neighbors Classifier")
#
## ---------- SGD Classifier -----------------
#from sklearn.linear_model import SGDClassifier
#from sklearn.multiclass import OneVsRestClassifier
#
#model = OneVsRestClassifier(SGDClassifier())
#y_pred, accuracy = run_model(model, "SGD Classifier")

# --------- Gaussian Naive Bayes ---------
from sklearn.naive_bayes import GaussianNB

model = GaussianNB(var_smoothing = 1e-1)
y_pred, accuracy = run_model(model, "Gaussian Naive Bayes")

## ----------- Neural network - Multi-layer Perceptron  ------------
#from sklearn.neural_network import MLPClassifier
#
#model = MLPClassifier()
#y_pred, accuracy = run_model(model, " MLP Neural network ")

print('done')
## create csv for submission
y_pred_bin = np.zeros([len(y_pred),2])
for i in range(len(y_pred)):
    y_pred_bin[i,0] = y_pred[i,1] + y_pred[i,4] + y_pred[i,5] + y_pred[i,6]
    y_pred_bin[i,1] = y_pred[i,0] + y_pred[i,2] + y_pred[i,3] 

output = []
output_bin = []

#add Headerline
output.append('image,MEL,NV,BCC,AKIEC,BKL,DF,VASC')
output_bin.append('image,BEN,MAL')

#get rid of .jpg
for i in range(len(testImages)):
    if testImages[i].endswith('.jpg'):
        testImages[i] = testImages[i][:-4]

for i in range(len(y_pred)):
    output.append(testImages[i] + 
        ',' + str(y_pred[i,0]) + 
        ',' + str(y_pred[i,1]) + 
        ',' + str(y_pred[i,2]) + 
        ',' + str(y_pred[i,3]) + 
        ',' + str(y_pred[i,4]) + 
        ',' + str(y_pred[i,5]) + 
        ',' + str(y_pred[i,6]))
    output_bin.append(testImages[i] + 
        ',' + str(y_pred_bin[i,0]) + 
        ',' + str(y_pred_bin[i,1]))
    
#save
np.savetxt('y_pred_' + featurematrix + '.csv',output,fmt='%s')
np.savetxt('y_pred_bin_' + featurematrix + '.csv',output_bin,fmt='%s')