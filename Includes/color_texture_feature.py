#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 11:00:46 2018

@author: veraschulz
"""
import matplotlib.pyplot as plt
import os
import cv2 
import numpy as np
import pandas as pd
from PIL import Image
import scipy
from scipy import ndimage, signal, stats
from skimage import feature
import mahotas as mt

# extraction of color features (HSV)
# mean, standard deviation and skewness for each HSV channel
# input: segmented image
# output: color feature vector

def colorhsv(image):
    
    image2 = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    meanh=np.mean(image2[:,:,0])
    means=np.mean(image2[:,:,1])
    meanv=np.mean(image2[:,:,2])
    stdh=np.std(image2[:,:,0])

    stds=np.std(image2[:,:,1])
    stdv=np.std(image2[:,:,2])
    skewh=stats.skew(image2[:,:,0].reshape(-1))
    skews=stats.skew(image2[:,:,1].reshape(-1))
    skewv=stats.skew(image2[:,:,2].reshape(-1))

    color=np.array([[meanh],[means],[meanv],[stdh],[stds],[stdv],[skewh],[skews],[skewv]])
    
    return color

#texture analysis
#GLCM: contrast, correlation, energy, homogeneity for each orientation (four angles)
#input: segmented grayscale image
    
#image1 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#
#def GLCM(image1):
#    GLCM = feature.texture.greycomatrix(image1, [1], [0, np.pi/2, np.pi, np.pi*(3/2)], levels=256, normed=True)
#    return GLCM
#    
#haralick texture
#input: segmented grayscale image
def haralick(image1):
  textures = mt.features.haralick(image1).ravel()
  return textures

def extract_features(image1):
    #calculate haralick features for 4 types of adjancency
    textures=mt.features.haralick(image1)
    
    #take the mean of it and return it
    ht_mean=textures.mean(axis=0)
    return ht_mean
    
#
##HOG features
## input: segmented grayscale image
## resizing of image might be necessary
## adapting hog descriptor might be necessary
#def hogf(image1):
#    hog = cv2.HOGDescriptor()
#    hogf = hog.compute(image1)
#    return hogf
#
