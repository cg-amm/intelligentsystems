# -*- coding: utf-8 -*-
'''
Spyder Editor

This is a temporary script file.
'''
import matplotlib.pyplot as plt
import os
import cv2 
import numpy as np
import pandas as pd
import decimal as d



def FeatureVec(images):

 #Finding hu moments
 def humom(images):
  im2=[]
  imhu=[]
  j=0
  for j in range(0,len(images)):
   im2.append(cv2.cvtColor(images[j],cv2.COLOR_BGR2GRAY))
   imhu.append(cv2.HuMoments(cv2.moments(im2[j])).flatten()) ## calculate moments 
     #List def of hu moments
  mo=np.array(imhu)
  mom=[]
  for m in range(0,7):
   mom.append(mo[:,m])
    #Normalized humoments list    
  mon=[]
  def normalize(lst):  #Normalize hu moments
     s = np.mean(lst)
     norm = [(float(i)-s)/np.std(lst) for i in lst]
     return norm 
  for i in range(0,7):
    mon.append(normalize(mom[i]))
  return mon         



    
 
#RGB Vector of Segmented Image
 def  rgbvec(images):
   img_rgb=[]
   img_rgbe=[]
   r=[]
   b=[]
   g=[]
   imgf=[]
  #Histogram equalisation
   def histogram_equalize(img):
    h, s, v = cv2.split(cv2.cvtColor(img,cv2.COLOR_BGR2HSV))
    V = cv2.equalizeHist(v)   
    return(cv2.cvtColor(cv2.merge((h, s, V)),cv2.COLOR_HSV2BGR )) 
    
   def rgbnormalization(img):
    b,g,r=cv2.split(img)
    b=np.matrix(b)
    g=np.matrix(g)
    r=np.matrix(r)
    b=np.sum(b)/(np.prod(b.shape)*255)
    g=np.sum(g)/(np.prod(g.shape)*255)
    r=np.sum(r)/(np.prod(r.shape)*255)
    return (b,g,r)   

   for i in range(0,len(images)):
    img_rgb.append(images[i])
    # convert the YUV image back to RGB format
    img_rgbe.append(histogram_equalize(img_rgb[i]))
    imgf.append(rgbnormalization(img_rgb[i]))
  
   imgf=np.array(imgf);colf=[]
   for i in range(0,3):
    colf.append(imgf[:,i])  
      
 
   return colf
    
 huf=humom(images)
 bgrf=rgbvec(images)

 com=bgrf+huf
 com=np.array(com)
 com=com.T

 return com
#Gradient descent classifier

