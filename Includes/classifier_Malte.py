#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 12:43:37 2018

@author: veraschulz
"""
#selection of most common classifiers and model function
import numpy as np
import pandas as pd
import numpy as np
import csv
import os
os.chdir(r'.\Includes')
# ------ Load Data -----------
# append features and labels of test dataset

import configparser

# open the configuration file and initialise variables
config = configparser.ConfigParser()
config.read(r'../Documents/init.ini')

#DEBUG
#featurematrix = config['Classifier']['FeatureMatrix']
#DEBUG END

path = (r"C:\Users\mkrai\Documents\ISIC2018_Task3_Training_Input")

def run_model(model, alg_name):
        # build the model on training data
        model.fit(train, train_labels)

        # make predictions for test data
        y_pred_proba = model.predict_proba(test)
        # calculate the accuracy score
        y_pred = model.predict(test)
        accuracy =  accuracy_score(test_labels, y_pred) * 100
        #print(report = classification_report(test_labels, y_pred))
        return y_pred_proba, accuracy

def initialize(path):
     # load image list

     os.chdir(path)
     images=[]
     for image in os.listdir(path):
          if image.endswith('.jpg') or image.endswith('.JPG'):
               images.append(image)
     
     return images 



images = initialize(path)
os.chdir(r"C:\Users\mkrai\Documents\ISIC2018_Task3_Training_GroundTruth")
# build train and test dataset
from sklearn.model_selection import train_test_split

# get groundtruth data from csv file and put it into a matrix
# first line is still the definition of the label (so skip one line)
with open('ISIC2018_Task3_Training_GroundTruth.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    labels1 = []
    labels2 = []
    labels3 = []
    labels4 = []
    labels5 = []
    labels6 = []
    labels7 = []
    
    for row in readCSV:
#        print(row[1],row[2],row[3],row[4],row[5],row[6],row[7])
#         labels = [row[1],row[2],row[3],row[4],row[5],row[6],row[7]]
         label1 = [row[1]]
         labels1.append(label1) 
         label2 = [row[2]]
         labels2.append(label2)
         label3 = [row[3]]
         labels3.append(label3)
         label4 = [row[4]]
         labels4.append(label4)
         label5 = [row[5]]
         labels5.append(label5)
         label6 = [row[6]]
         labels6.append(label6)
         label7 = [row[7]]
         labels7.append(label7)
    
    labels = [labels1,labels2,labels3,labels4,labels5,labels6,labels7]
    labels = list(map(list, zip(*labels)))
    labels = np.asarray(labels)
    labels = labels[1:len(labels),:]

    labelsArray = []
    for i in range(len(labels)):
        temp = np.argmax(labels[i,:]) + 1
        labelsArray.append(temp)

## Split Train/Test set
# read in the csv file
file = open('../intelligentsystems/Documents/val_split_info.csv', "r")
csv_reader = csv.reader(file, delimiter=",")
validation =[]
i = 0
for row in csv_reader:
   row = row[0] + '.jpg'  # for comparison with images
   validation.append(row)
   i = i + 1
file.close()

# get the indices of train and test set
validation = validation[1:len(validation)]
valindex = []
trainindex = []
for i in range(len(validation)):
    for j in range(len(images)):
        if validation[i] == images[j]: 
            valindex.append(j)
        else:
             trainindex.append(j)
             
for i in range(1):
    #if i == 0:
     #   featurematrix ='comcolortex_with_Hairremoval_and_with_Segmentmole_v3.npy'
    #if i == 1:
    featurematrix ='comcolortex_without_Hairremoval_and_without_Segmentmole_v3.npy'
    ## Load Featruematrix
    com = np.load('C:/Users/mkrai/Documents/intelligentsystems/Documents/' + featurematrix)

    # fill the train and test set            
    val_labels = []
    val = [] 
    for ind in valindex: 
        val_labels.append(labelsArray[ind])
        val.append(com[ind])
        
    train = [] 
    train_labels = [] 
    for ind in trainindex: 
        train_labels.append(labelsArray[ind])
        train.append(com[ind])

    #rename the found train and test set
    test = np.asarray(val)
    test_labels =val_labels
    train = np.asarray(train)

    from sklearn.metrics import accuracy_score
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.metrics import classification_report

    # -------- Nearest Neighbors ----------
    print('start nearest Neighbors training')
    from sklearn import neighbors
    model = neighbors.KNeighborsClassifier(n_neighbors=10, weights = 'uniform', algorithm='auto')
    y_pred, accuracy = run_model(model, "Nearest Neighbors Classifier")

    print('done with machine learning part')
    ## create csv for submission
    y_pred_bin = np.zeros([len(y_pred),2])
    for i in range(len(y_pred)):
        y_pred_bin[i,0] = y_pred[i,1] + y_pred[i,4] + y_pred[i,5] + y_pred[i,6]
        y_pred_bin[i,1] = y_pred[i,0] + y_pred[i,2] + y_pred[i,3] 

    output = []
    output_bin = []

    #add Headerline
    output.append('image,MEL,NV,BCC,AKIEC,BKL,DF,VASC')
    output_bin.append('image,BEN,MAL')

    #get rid of .jpg
    for i in range(len(validation)):
        if validation[i].endswith('.jpg'):
            validation[i] = validation[i][:-4]

    for i in range(len(y_pred)):
        output.append(validation[i] + 
            ',' + str(y_pred[i,0]) + 
            ',' + str(y_pred[i,1]) + 
            ',' + str(y_pred[i,2]) + 
            ',' + str(y_pred[i,3]) + 
            ',' + str(y_pred[i,4]) + 
            ',' + str(y_pred[i,5]) + 
            ',' + str(y_pred[i,6]))
        output_bin.append(validation[i] + 
            ',' + str(y_pred_bin[i,0]) + 
            ',' + str(y_pred_bin[i,1]))
        
    #save
    np.savetxt('y_pred_' + featurematrix + '.csv',output,fmt='%s')
    np.savetxt('y_pred_bin_' + featurematrix + '.csv',output_bin,fmt='%s')
    