# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import matplotlib.pyplot as plt
import os
import cv2 
import numpy as np
import pandas as pd
import decimal as d


path2  = (r"..\ISIC2018_Task3_Training_Input")

images=[]
for image in os.listdir(path2):
     if image.endswith('.jpg') or image.endswith('.JPG'):
          images.append(image)
          

os.chdir( path )

#Finding hu moments
im2=[];imhu=[];j=0;
for j in range(0,5):
  
  im2.append(cv2.cvtColor(cv2.imread(images[j]), cv2.COLOR_BGR2GRAY))
  imhu.append(cv2.HuMoments(cv2.moments(im2[j])).flatten()) ## calculate moments 
  
#List def of hu moments
mo=np.array(imhu)
mom=[];
for m in range(0,7):
    mom.append(mo[:,m])

#Normalize hu moments
def normalize(lst):
    s = np.mean(lst)
    norm = [float(i)/s for i in lst]
    return norm    

#Normalized humoments list    
mon=[]
for i in range(0,7):
    mon.append(normalize(mom[i]))
    

    
    